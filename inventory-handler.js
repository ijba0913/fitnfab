const ObjectId = require('mongodb').ObjectID

function handleInventoryTasks(app, db) {

  const cart = db.collection("cart")
  const inventory = db.collection("inventory")
  const orders = db.collection("orders")
  const products = db.collection("products")
  const users = db.collection("users")

  app.get('/getInventory', (request, response) => {
    inventory.find().toArray((error, inventory) => {
      response.json(inventory)
    })
  })

  app.get('/getInventoryByProductId/:productId', (request, response) => {
    const id = ObjectId(request.params.productId)

    inventory.findOne({ product_id: id }, (error, result) => {
      // console.log(error, 'kk', inventory)
      response.json(result)
    })
  })

  app.post('/addInventoryQty/:productId', (request, response) => {
    const id = ObjectId(request.params.productId)
    const qty = request.body.newQty

    inventory.update({ product_id: id }, { $set: { qty_inventory: qty } })
  })

  app.post('/subtractInventoryQuantity', (request, response) => {
    const cart = request.body.cart

    for (const item of cart) {
      const id = ObjectId(item.product._id)
      inventory.findOne({ product_id: id }, (error, invResult) => {
        const newQty = invResult.qty_inventory - item.qty_cart
        inventory.updateOne({ product_id: id }, { $set: { qty_inventory: newQty } })
      })
    }


  })

  app.post('/deleteInventory/:productId', (request, response) => {
    inventory.deleteOne({ product_id: ObjectId(request.params.productId) })
  })
}

module.exports = handleInventoryTasks;