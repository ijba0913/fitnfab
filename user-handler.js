const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')

function extractJWT(header) {
  return header.slice(7)
}

function handleUserTasks(app, db) {

  const cart = db.collection("cart")
  const inventory = db.collection("inventory")
  const orders = db.collection("orders")
  const products = db.collection("products")
  const users = db.collection("users")

  app.post('/signUpUser', (request, response) => {
    const user = request.body
    console.log('uuu', user)

    users.findOne({ username: user.username }, (error, userResult) => {
      if (userResult === null) {
        encryptPassword(user.password, (hashedPassword) => {
          user.password = hashedPassword
          users.insertOne(user, (error, result) => {
            response.json({
              success: true,
              message: `User Succesfully Signed Up. Sign in to your account now.`
            })
          })
        })
      }
      else {
        response.json({
          success: false,
          message: `Username Taken`
        })

      }
    })
  })

  app.get('/currentUser', (request, response) => {
    const authHeader = request.header('Authorization')

    jwt.verify(extractJWT(authHeader), 'ijba0913', (error, decoded) => {
      console.log(decoded, 'ecode')
      if (decoded) {

        response.json({
          success: true,
          message: `User Verified`,
          currentUser: decoded.user
        })
      }
      else {
        response.json({
          success: false,
          message: `User Not Verified`
        })
      }
    })

  })

  app.get('/getUsers', (request, response) => {
    users.find().toArray((error, result) => {
      response.json(result)
    })
  })
}

function encryptPassword(password, command) {
  bcrypt.genSalt(12, (saltError, salt) => {
    if (saltError) {
      console.log(saltError);
    }
    else {
      bcrypt.hash(password, salt, (hashError, hashedPassword) => {
        if (hashError) {
          console.log(hashError);
        }
        else {
          command(hashedPassword);
        }
      })
    }
  })
}

module.exports = handleUserTasks;