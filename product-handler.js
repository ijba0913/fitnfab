const ObjectId = require('mongodb').ObjectID

function handleProductTasks(app, db) {
  const cart = db.collection("cart")
  const inventory = db.collection("inventory")
  const infos = db.collection("infos")
  const orders = db.collection("orders")
  const products = db.collection("products")
  const users = db.collection("users")

  app.post('/addProduct', (request, response) => {

    console.log(request.body, 'input')
    infos.insertOne({
      shortInfo: request.body.shortInfo,
      longInfo: request.body.longInfo
    }, (infoError, info) => {

      products.insertOne({
        name: request.body.name,
        info_id: info.insertedId,
        price: request.body.price,
        productSolutionId: request.body.productSolutionId
      }, (productError, product) => {

        inventory.insertOne({
          product_id: product.insertedId,
          qty_inventory: "0"
        }, (error, r) => {
        })
      })
    })
  })

  app.get('/getProducts', (request, response) => {
    products.find({}).toArray((error, products) => {
      response.json(products)
    })
  })

  app.get('/getProductsByName/:name', (request, response) => {
    const name = new RegExp(`${request.params.name}`, "i")

    products.find({ name: { $regex: name } }).toArray((error, products) => {
      if (error) throw error
      response.json(products)
    })
  })

  app.get('/getProductById/:productId', (request, response) => {
    const id = ObjectId(request.params.productId)

    products.findOne({ _id: id }, (error, product) => {
      if (error) throw error
      response.json(product)
    })
  })

  app.get('/getProductsByProductSolution/:productSolution', (request, response) => {
    products.find({ productSolutionId: request.params.productSolution }).toArray((error, product) => {
      response.json(product)
    })
  })

  app.post('/updateProduct/:productId', (request, response) => {
    const id = ObjectId(request.params.productId)

    products.update({ _id: id }, {
      $set: {
        name: request.body.name,
        price: request.body.price,
        productSolutionId: request.body.productSolutionId,
      }
    }, (error, result) => {
      products.findOne({ _id: id }, (error, product) => {
        infos.update({ _id: product.info_id }, {
          $set: {
            shortInfo: request.body.shortInfo,
            longInfo: request.body.longInfo,
          }
        })
      })

    })
  })

  app.get('/getInfosById/:infoId', (request, response) => {
    infos.findOne({ _id: ObjectId(request.params.infoId) }, (error, result) => {
      response.json(result)
    })
  })

  app.post('/deleteProduct/:productId', (request, response) => {
    const id = ObjectId(request.params.productId)

    products.find({ _id: id }, (findError, product) => {
      infos.deleteOne({ _id: ObjectId(product.info_id) })
      inventory.deleteOne({ product_id: id })
      products.deleteOne({ _id: id })
    })
  })
}

module.exports = handleProductTasks;