const ObjectId = require('mongodb').ObjectID

function handleCartTasks(app, db) {
  const cart = db.collection("cart")
  const inventory = db.collection("inventory")
  const orders = db.collection("orders")
  const products = db.collection("products")
  const users = db.collection("users")

  app.post('/addToCart/:productId', (request, response) => {
    const id = ObjectId(request.params.productId)
    const qty_inventory = request.body.qty_inventory

    products.findOne({ _id: id }, (error, product) => {
      cart.insert({ product: product, qty_cart: "1" }, (cartError, result) => {
        response.json({
          success: true,
          message: `Product Added to Cart`
        })
      })
    })
  })

  app.get('/getCart', (request, response) => {
    cart.find().toArray((error, cart) => {
      response.json(cart)
    })
  })

  app.get('/getCartByProductId/:productId', (request, response) => {
    cart.findOne({ "product._id": ObjectId(request.params.productId) }, (error, cart) => {
      response.json(cart)
    })
  })

  app.post('/updateCartQty/:productId', (request, response) => {
    const id = ObjectId(request.params.productId)
    const qty = request.body.newQty

    inventory.findOne({ product_id: id }, (error, inventory) => {
      if (qty > inventory.qty_inventory || qty <= 0) {
        response.json({
          success: false,
          message: `Insufficient`
        })
      }
      else {
        cart.update({ "product._id": id }, { $set: { qty_cart: qty } }, (error, result) => {
          response.json({ success: true })
        })

      }
    })
  })

  app.post('/deleteProductInCart/:productId', (request, response) => {
    cart.deleteOne({ "product._id": ObjectId(request.params.productId) })
  })

  app.post('/emptyCart', (request, response) => {
    cart.drop()
  })

}

module.exports = handleCartTasks;