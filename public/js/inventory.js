function displayInventoryList(url) {
  doAjax('GET', url, {}, (products) => {
    const container = document.getElementById('inventoryTable')
    container.innerHTML = ``

    for (const product of products) {
      if (product.qty_inventory <= 0) {

      }

      container.innerHTML += getInventoryProductHtml(product)
      setProductInvQty(product._id)
    }
  })
}

function getInventoryProductHtml(product) {
  console.log('inv qty', product.qty_inventory)
  return `<tr id="invRow${product._id}">
            <th><center><div class="btn-group ">
                <button class="btn btn-danger" onclick="deleteProduct('${product._id}')" >
                  <img class="glyph" src="glyph/svg/si-glyph-trash.svg"/>
                </button>
                <button type="button" class="btn btn-warning"   data-toggle="modal" data-target="#formProductModal"
                 onclick="setEditProductModal('${product._id}')">
                  <img  class="glyph" src="glyph/svg/si-glyph-circle-info.svg"/>
                </button> 
                <button class="btn btn-success" data-toggle="modal" data-target="#editQuantityModal"
                 onclick="setAddInvQtyModal('${product._id}', '${product.name}')">
                  <img  class="glyph" src="glyph/svg/si-glyph-button-plus.svg"/>
                </button> 
              </div></center></th>
            <td><span id="productInvQty${product._id}"></span>              
            </td>
            <td>${product.name}</td>
            <td>${product.price}</td>
          </tr>`
}


function setProductInvQty(id) {
  doAjax('GET', `/getInventoryByProductId/` + id, {}, (inventory) => {
    document.getElementById(`productInvQty${id}`).innerHTML = inventory.qty_inventory
    if (inventory.qty_inventory <= 0) {
      document.getElementById(`invRow${id}`).className = 'text-red'
    }
  })
}

function setOnViewInvDetails(id, name) {
  doAjax('GET', `/getInventoryByProductId/` + id, {}, (inventory) => {
    document.getElementById('onViewProductInvQtyValue').value =
      inventory.qty_inventory

    document.getElementById('onViewProductInvQty').innerHTML =
      inventory.qty_inventory + ` Orders Are Still Available`
    $('#buyPageProductModal').modal('show')

    if (inventory.qty_inventory <= 0) {
      // document.getElementById('addToCartBtn').disabled = true

      $('#addToCartBtn').prop('disabled', true);
      alert(`${name} is Out of Stock`)
      document.getElementById('onViewProductInvQty').innerHTML = `OUT OF STOCK!`




    }
  })
}

function setAddInvQtyModal(id, name) {
  document.getElementById('editQuantityModalTitle').innerHTML = name
  document.getElementById('editQuantityProductId').value = id
  document.getElementById('editQuantityValue').value = "1"
  document.getElementById('inventoryInfo').innerHTML = ""

  doAjax('GET', `/getInventoryByProductId/` + id, {}, (inventory) => {
    document.getElementById('currentQuantity').value = inventory.qty_inventory
  })
}

function addInvQty() {
  const id = document.getElementById('editQuantityProductId').value
  const currentQty = parseInt(document.getElementById('currentQuantity').value)
  const editQtyValue = parseInt(document.getElementById('editQuantityValue').value)
  const newQty = currentQty + editQtyValue

  console.log(currentQty, 'ee', editQtyValue)
  doAjax('POST', '/addInventoryQty/' + id, { newQty }, (result) => { })
}

function subtractInvQty(cart) {
  doAjax('POST', '/subtractInventoryQuantity', { cart }, (result) => { })
}