function confirmOrder(user) {
  doAjax('GET', '/getCart', {}, (cart) => {
    const totalPrice = computeTotalPrice(cart)
    // const totalPrice = 0

    const now = new Date()
    const time = now.toLocaleTimeString()
    const date = now.toDateString()

    doAjax('POST',
      `/saveOrder`, {
        username: user.username,
        items: cart,
        totalPrice,
        date,
        time
      }, (result) => {
        if (result.success) {
          subtractInvQty(cart)
          alert(result.message)
          emptyCart()
        }
      })
  })
}

function displayOrders() {
  doAjax('GET', '/getOrders', {}, (orders) => {
    let orderBody = ''
    console.log(orders, 'oredersss')

    for (const order of orders) {
      orderBody += getOrderHtml(order)
    }

    const container = document.getElementById('ordersList')
    container.innerHTML = `<div class="list-group">${orderBody}</div>`
  })
}

function getOrderHtml(order) {
  return `<a href="#" class="list-group-item list-group-item-action flex-column align-items-start p-4">
            <div class="d-flex w-100 justify-content-between">
              <h5 class="mb-3">Order</h5>
              <small class="text-muted">${order.date} ${order.time}</small>
            </div>
            <table class="table table-sm mb-1 ">
              <thead class="thead-dark ">
                <tr>
                  <th scope="col">Qty</th>
                  <th scope="col">Product</th>
                  <th scope="col">Price</th>
                </tr>
              </thead>
              <tbody>
              ${getItemTable(order.items)}
              </tbody>
            </table>
            <h6 class="text-right mr-5 pr-4 mt-2"><strong>Total</strong> ${order.totalPrice}</h6><br>
          <small class="text-muted">${order.username}</small></a>`
}

function getItemTable(items) {
  let itemTable = ''
  for (const item of items) {
    itemTable += `<tr>
                    <td>${item.qty_cart}</td>
                    <td>${item.product.name}</td>
                    <td>${item.product.price}</td>
                  </tr>`
  }
  return itemTable
}