function saveProduct() {

  let url = `/addProduct`

  if (document.getElementById('formProductCommand').value === 'update') {
    url = `/updateProduct/` + document.getElementById('formProductId').value
  }

  doAjax('POST', url, getProductInput(), (result) => { })
  displayInventoryList('/getProducts')
}


function displayProductList(url) {
  doAjax('GET', url, {}, (products) => {
    const container = document.getElementById('buyPageProductList')
    const numColumns = 3
    const numRows = parseInt(products.length / numColumns) + 1
    container.innerHTML = ''

    for (let i = 0; i < numRows; i++) {
      let string = '';

      for (let j = 0; j < numColumns; j++) {
        const index = (numColumns * i) + j
        const product = products[index]

        if (product !== undefined) {
          string += getProductHtml(product)
          setProductShortInfo(product.info_id)
        }
      }
      container.innerHTML += `<div class="row">${string}</div><br>`
      console.log(container.innerHTML)
    }
  })
}

function getProductHtml(product) {
  return `<div class="col-md-4 products">
            <a data-toggle="modal" data-target="#buyPageProductModal"  href="" 
              onclick="setViewProductModal('${product._id}')" >
               <img class="image-product" src="images/${product.name}.jpg"  /> 
            </a><br>
            <span class="small">
              <strong>${product.name}</strong><br><br>
              <em id="shortInfo${product.info_id}">
                 </em><br>
            </span>
          </div>`
}

function setProductShortInfo(infoId) {
  doAjax('GET', `/getInfosById/` + infoId, {}, (infos) => {
    document.getElementById('shortInfo' + infoId).innerHTML = infos.shortInfo
  })
}

function setViewProductModal(id) {
  doAjax('GET', `/getProductById/` + id, {}, (product) => {
    document.getElementById('buyPageProductModalTitle').innerHTML = product.name
    document.getElementById('viewProductModalBody').innerHTML = `<div class="row">
      <div class="col-lg-5">  
        <img class="modal-product-image" src="images/${product.name}.jpg" />  
      </div>
      <div class="col-lg-7 p-5 ">
        <span id="onViewProductLongInfo"></span><br><br>
        <strong><h5>₱ ${product.price}</h5></strong>
        <input type="hidden" id="onViewProductId" value="${id}">
        <input type="hidden" id="onViewProductName" value="${product.name}">
        <input type="hidden" id="onViewProductInvQtyValue">
        <u><span id="onViewProductInvQty"></span></u>
      </div></div>`

    setOnViewProductLongInfo(product.info_id)
    setAddToCartBtn(id, product.name)
  })
}

function setOnViewProductLongInfo(infoId) {
  doAjax('GET', `/getInfosById/` + infoId, {}, (infos) => {
    console.log('rrrr', infos)
    document.getElementById('onViewProductLongInfo').innerHTML = infos.longInfo
  })
}

function setAddProductModal() {
  document.getElementById('formProductCommand').value = 'add'
  document.getElementById('formProductModalTitle').innerHTML = 'Add New Product'
  document.getElementById('formProductName').value = ''
  document.getElementById('formProductSolutionId').value = '1'
  document.getElementById('formProductShortInfo').value = ''
  document.getElementById('formProductLongInfo').value = ''
  document.getElementById('formProductPrice').value = ''
}

function setEditProductModal(id) {
  document.getElementById('formProductId').value = id
  document.getElementById('formProductModalTitle').innerHTML = 'Edit Product'
  document.getElementById('formProductCommand').value = 'update'

  doAjax('GET', `/getProductById/` + id, {}, (product) => {
    document.getElementById('formProductName').value = product.name
    document.getElementById('formProductSolutionId').value = product.productSolutionId
    document.getElementById('formProductPrice').value = product.price
    doAjax('GET', `/getInfosById/` + product.info_id, {}, (infos) => {
      document.getElementById('formProductShortInfo').value = infos.shortInfo
      document.getElementById('formProductLongInfo').value = infos.longInfo
    })
  })
}

function getProductInput() {
  return {
    name: document.getElementById('formProductName').value,
    shortInfo: document.getElementById('formProductShortInfo').value,
    longInfo: document.getElementById('formProductLongInfo').value,
    productSolutionId: document.getElementById('formProductSolutionId').value,
    price: document.getElementById('formProductPrice').value
  }
}

function deleteProduct(id) {
  if (confirm("Confirm Deletion of this Product?")) {
    doAjax('POST', `/deleteProduct/` + id, {}, (data) => {
      displayInventoryList()
    })
  }
}