const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt');

function handleSignInTasks(app, db) {
  const users = db.collection("users")

  app.post('/signIn', (request, response) => {
    const username = request.body.username
    const password = request.body.password
    console.log(request.body)

    users.findOne({ username: username }, (error, user) => {
      if (user) {
        bcrypt.compare(password, user.password, (error, verified) => {
          if (verified) {
            jwt.sign({ user }, 'ijba0913', (error, token) => {
              console.log(token, 'll')
              response.json({
                success: true,
                message: `User Signed In`,
                theToken: token
              })
            })
          }
          else {
            response.json({
                success: false,
                message: `Invalid Password Input`
              })
          }
        })
      }
      else {
        response.json({
          success: false,
          message: `User Doesn't Exist`
        })
      }
    })
  })
}

module.exports = handleSignInTasks; 
