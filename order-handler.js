const ObjectId = require('mongodb').ObjectID

function handleOrderTasks(app, db) {
  const cart = db.collection("cart")
  const orders = db.collection("orders")
  const users = db.collection("users")

  app.post('/saveOrder', (request, response) => {
    console.log(request.body)
    orders.insertOne({
      items: request.body.items,
      username: request.body.username,
      totalPrice: request.body.totalPrice,
      date: request.body.date,
      time: request.body.time
    }, (error, result) => {
      response.json({
        success: true,
        message: `Order Saved`
      })
    })
  })

  app.get('/getOrders', (request, response) => {
    orders.find().toArray((error, result) => {
      response.json(result)
    })
  })

  app.get('/getOrdersByUser/:username', (request, response) => {
    orders.find({ username: request.params.username0 }).toArray((error, orders) => {
      response.json(orders)
    })
  })
}

module.exports = handleOrderTasks