function doAjax(method, url, data, command) {
  const xhr = new XMLHttpRequest();

  xhr.onreadystatechange = () => {
    if (xhr.status === 200 && xhr.readyState === 4) {
      command(JSON.parse(xhr.responseText));
    }
  }

  xhr.open(method, url, true);

  if (method === 'POST') {
    xhr.setRequestHeader('Content-Type', 'application/json')
    xhr.send(JSON.stringify(data));
  }
  else {
    if (url === '/currentUser') {
      xhr.setRequestHeader('Authorization', `Bearer ${localStorage.getItem('theJwt')}`)
    }
    xhr.send();
  }
}