window.onload = () => {
  doAjax('GET', '/currentUser', {}, (result) => {
    // console.log(result.message)
    // console.log(result.success)
    // user is currently signed in
    console.log(result)
    if (result.success) {
      const user = result.currentUser
      setStyles()

      // nav links
      const navBuy = document.getElementById('navBuy')
      const navInventory = document.getElementById('navInventory')
      const navSales = document.getElementById('navSales')

      // user dropdown buttons
      const signOutBtn = document.getElementById('signOutBtn')

      document.getElementById('navbarDropdown').innerHTML = `Welcome ${user.firstName}`
      document.getElementById('signPage').style.display = "none"

      // view the main page
      showBuyPage(user)

      // to view buy page 
      navBuy.onclick = () => {
        showBuyPage(user)
      }

      //to view inventory page
      navInventory.onclick = () => {
        autoEmptyCart(user, showInventoryPage, () => { })
        // showInventoryPage()
      }

      // to view sales page
      navSales.onclick = () => {
        showSalesPage()

      }

      // to sign out
      signOutBtn.onclick = () => {
        autoEmptyCart(user, showSignInPage, () => {
          localStorage.clear()
          window.location.reload()
        })

      }

    }
    // user is not signed in
    else {
      showSignInPage()
    }
  })
}

function showSignInPage() {
  // hide navbar
  document.getElementById('navBar').style.visibility = "hidden"

  // to view sign page
  setContainer('signForm', 'signInForm')

  // buttons and links
  const signInBtn = document.getElementById('signInBtn')
  const signUpLink = document.getElementById('signUpLink')

  // to sign in the user
  signInBtn.onclick = (event) => {
    signInUser()
  }

  // to show sign up form
  signUpLink.onclick = (event) => {
    showSignUpPage()
  }
}

function showSignUpPage() {

  // to view sign up form
  setContainer('signForm', 'signUpForm')

  // button
  const signUpBtn = document.getElementById('signUpBtn')

  // set css of sign up page
  setSignUpUI()

  // to sign up the new user
  signUpBtn.onclick = (event) => {

    const user = {
      username: document.getElementById('signUpUsername').value,
      firstName: document.getElementById('signUpFirstName').value,
      lastName: document.getElementById('signUpLastName').value,
      password: document.getElementById('signUpPassword').value,
      position: document.getElementById('signUpPosition').value
    }

    if (user.username !== '' && user.firstName !== '' && user.lastName !== '' &&
      user.password !== '' && user.position !== '') {

      // save user in db
      signUpUser(user)

      // show sign up page
      showSignUpPage()

    }
    else {
      window.alert('Please Fill Up The Form Completely')
    }
  }
}

function showBuyPage(user) {
  // set the container
  setContainer('mainContainer', 'buyPage')

  // buttons
  const showAllBtn = document.getElementById('buyPageShowAllBtn')
  const emptyCartBtn = document.getElementById('emptyCartBtn')
  const confirmOrderBtn = document.getElementById('confirmOrderBtn')
  const addToCartBtn = document.getElementById('addToCartBtn')
  const confirmEditQtyBtn = document.getElementById('confirmEditQtyBtn')

  // input
  const searchBar = document.getElementById('buyPageSearchProductInput')

  // to view all products
  displayProductList('/getProducts')

  // to display cart
  displayCart()

  // to view products by product solution 
  for (let i = 1; i <= 4; i++) {
    document.getElementById(`productSol${i}Btn`).onclick = () => {
      const url = '/getProductsByProductSolution/' + i
      displayProductList(url)
    }
  }

  // to view search product by name
  searchBar.onkeyup = () => {
    const url = '/getProductsByName/' +
      encodeURIComponent(document.getElementById('buyPageSearchProductInput').value);

    displayProductList(url)

    if (searchBar.value === '') {
      displayProductList('/getProducts')
    }
  }

  // to show all products
  showAllBtn.onclick = () => {
    displayProductList('/getProducts')
  }

  // to confirm edit cart qty
  confirmEditQtyBtn.onclick = () => {
    editProductCartQty()
  }

  // add to cart
  addToCartBtn.onclick = () => {
    addToCart()
    displayProductList('/getProducts')
  }

  // to empty cart
  emptyCartBtn.onclick = () => {
    emptyCart()
  }

  confirmOrderBtn.onclick = () => {
    if (confirm("Are You Sure to Buy this order?")) {
      confirmOrder(user)
    }
  }

}

function showInventoryPage() {
  // set the contianer
  setContainer('mainContainer', 'inventoryPage')

  // buttons
  const addProductBtn = document.getElementById('addProductBtn')
  const saveProductBtn = document.getElementById('saveProductBtn')
  const confirmEditQtyBtn = document.getElementById('confirmEditQtyBtn')

  // input
  const inventorySearchBar = document.getElementById('inventorySearchProductInput')

  // to view inventory list
  displayInventoryList('/getProducts')

  // to view search product by name
  inventorySearchBar.onkeyup = () => {
    const url = '/getProductsByName/' +
      encodeURIComponent(inventorySearchBar.value)

    displayInventoryList(url)

    if (inventorySearchBar.value === '') {
      displayInventoryList('/getProducts')
    }
  }

  // to view add product modal
  addProductBtn.onclick = () => {
    setAddProductModal()
  }

  // to save product in db
  saveProductBtn.onclick = () => {
    saveProduct()
    displayInventoryList('/getProducts')
  }

  // to add quantity
  confirmEditQtyBtn.onclick = () => {
    addInvQty()
    displayInventoryList('/getProducts')
  }
}

function showSalesPage() {
  // set container to sales page 
  setContainer('mainContainer', 'salesPage')

  // view order history
  displayOrders()
}

function setContainer(container, template) {
  event.preventDefault()
  const html = document.getElementById(template).innerHTML
  document.getElementById(container).innerHTML = html
}