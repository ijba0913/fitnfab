
function setStyles() {


  const screenHeight = window.innerHeight
  const screenWidth = window.innerWidth
  const navBar = document.getElementById("navBar")
  const navBarHeight = navBar.clientHeight

  const mainContaner = document.getElementById('mainContainer')
  mainContaner.style.height = `${screenHeight - navBarHeight}px`
  // console.log('height', mainContaner.clientHeight)

  window.onresize = () => {
    const screenHeight = window.innerHeight
    const screenWidth = window.innerWidth
    const navBar = document.getElementById("navBar")
    const navBarHeight = navBar.clientHeight

    const mainContaner = document.getElementById('mainContainer')
    mainContaner.style.height = `${screenHeight - navBarHeight}px`
    // console.log('height', mainContaner.clientHeight)

  }
}

function pageTransition() {
  document.getElementById('signPage').style.height = "0px"
  document.getElementById('logo').style.width = "149px"
  document.getElementById('logo').style.height = "30px"
  document.getElementById('logo').style.position = "fixed"
  document.getElementById('logo').style.marginTop = "-100px"
  document.getElementById('logo').style.top = "-20px"
  document.getElementById('logo').style.opacity = "0"
  document.getElementById('navBar').style.visibility = "visible"
}

function setSignUpUI() {
  document.getElementById('signContainer').style.marginTop = `70px`

  document.getElementById('logo').style.marginTop = "80px"
  document.getElementById('signUpForm').style.marginTop = "0px"
  if (window.innerWidth < 768) {
    document.getElementById('logo').style.marginTop = "0px"
  }
  window.onresize = () => {

    document.getElementById('logo').style.marginTop = "80px"
    document.getElementById('signUpForm').style.marginTop = "0px"

    if (window.innerWidth < 768) {
      document.getElementById('logo').style.marginTop = "0px"
      document.getElementById('signUpForm').style.marginTop = "10px"
    }
  }

}
