const solver = require('./../public/js/solver')
const assert = require('chai')

const cart = [
  {
    product: { price: 1000 },
    qty_cart: 2
  },
  {
    product: { price: 1000 },
    qty_cart: 1
  }
]

describe('solver', () => {
  it('summate the product of price and qauntity', () => {
    const actual = solver(cart)
    assert.expect(actual).to.equal(3000)
  })
})