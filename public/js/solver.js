function computeTotalPrice(cart) {
  let totalPrice = 0

  for (const item of cart) {
    totalPrice += item.qty_cart * item.product.price
  }

  return totalPrice
}

module.exports = computeTotalPrice