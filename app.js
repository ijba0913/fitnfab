const { MongoClient } = require('mongodb')
const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')
const jwt = require('jsonwebtoken')

const url = "mongodb://localhost:27017/finalProject"
const port = 8084
const app = express()

const handleSignInTasks = require('./signin-handler')
const handleUserTasks = require('./user-handler')
const handleProductTasks = require('./product-handler')
const handleInventoryTasks = require('./inventory-handler')
const handleCartTasks = require('./cart-handler')
const handleOrderTasks = require('./order-handler')
const handleInfoTasks = require('./info-handler')

app
  .use(bodyParser.urlencoded({ extended: true }))
  .use(bodyParser.json())
  .use(express.static(path.join(process.cwd(), 'public')))

MongoClient.connect(url, (error, db) => {
  if (error) {
    console.log(error)
  }
  else {
    handleUserTasks(app, db)
    handleSignInTasks(app, db)
    handleProductTasks(app, db)
    handleInventoryTasks(app, db)
    handleCartTasks(app, db)
    handleOrderTasks(app, db)
    handleInfoTasks(app, db)
  }
 })

app.listen(port, () => {
  console.log(`Server deployed at http://localhost:${port}`)
})



  //PRODUCTS
  // app.post('/add-product', (request, response) => {
  //   products.insertOne({
  //     name: request.body.name,
  //     shortInfo: request.body.shortInfo,
  //     longInfo: request.body.longInfo,
  //     price: request.body.price,
  //     productSolutionID: request.body.productSolutionID
  //   }, (error, product) => {
  //     inventory.insertOne({ product_id: product.insertedId, qty_inventory: "0" })
  //   })
  // })

  // app.get('/search-product/:name', (request, response) => {
  //   inventory.find({
  //     "product.name": { $regex: new RegExp(`${request.params.name}`, "i") }
  //   }).toArray((error, result) => {
  //     response.json(result)
  //   })
  // })

  // app.get('/search-product_solution/:productSolutionID', (request, response) => {
  //   inventory.find(
  //     { "product.productSolutionID": request.params.productSolutionID }
  //   ).toArray((error, result) => {
  //     response.json(result)
  //   })
  // })

  // app.post('/update-product', (request, response) => {
  //   const id = new mongo.ObjectID(request.body.id)

  //   const newUpdate = {
  //     _id: id,
  //     name: request.body.name,
  //     shortInfo: request.body.shortInfo,
  //     longInfo: request.body.longInfo,
  //     price: request.body.price,
  //     productSolutionID: request.body.productSolutionID,
  //   }
  // })

  // app.post('/delete-product', (request, response) => {
  //   const id = new mongo.ObjectID(request.body.id)

  //   inventory.deleteOne({ "product._id": id })
  //   products.deleteOne({ _id: id })
  // })

  // // INVENTORY
  // app.post('/update-inventory-quantity', (request, response) => {
  //   cart.find().toArray((error, cartResult) => {
  //     inventory.insertOne({ product: result, qty_inventory: "0" })
  //   })
  // })

  // app.get('/inventory', (request, response) => {
  //   inventory.find().sort({"_id":1}).toArray((error, inventoryResults) => {

  //     let productIDs = []
  //     for (const item of inventoryResults) {
  //       productIDs.push(new mongo.ObjectID(item.product_id))
  //     }

  //     products.find({ _id: { $in: productIDs } }).toArray((error, productResults) => {

  //       let results = []
  //       for (let i = 0; i < inventoryResults.length; i++) {
  //         results.push({ product: productResults[i], qty_inventory: inventoryResults[i].qty_inventory })
  //       }

  //       response.json(results)
  //     })
  //   })
  // })

  // app.post('/update-inventory', (request, response) => {
  //   const id = new mongo.ObjectID(request.body.id)
  //   const qty = request.body.newQty
  //   inventory.update({ product_id: id }, { $set: { qty_inventory: qty } })
  // })


  // app.get('/search-inventory/:name', (request, response) => {
  //   const name = new RegExp(`${request.params.name}`, "i")

  //   inventory.find({
  //     "product.name": { $regex: name }
  //   }).toArray((error, inventory) => {
  //     if (error) throw error
  //     response.json(inventory)
  //   })
  // })

  // app.get('/search-inventory_product_id/:id', (request, response) => {
  //   const id = new mongo.ObjectID(request.params.id)

  //   inventory.findOne({
  //     "product._id": id
  //   }, (error, inventory) => {
  //     if (error) throw error
  //     response.json(inventory)
  //   })
  // })

  // // CART
  // app.post('/add-to-cart', (request, response) => {
  //   const id = new mongo.ObjectID(request.body.id)
  //   const qty_inventory = request.body.qty_inventory

  //   cart.findOne({ product_id: id }, (error, cartResults) => {
  //     if (cartResults !== null) {
  //       response.json({ success: false })
  //     }
  //     else {
  //       products.findOne({ _id: id }, (error, productResults) => {
  //         cart.insert({ product_id: id, qty_cart: "1" })
  //         response.json({ success: true })
  //       })
  //     }
  //   })
  // })

  // app.get('/cart', (request, response) => {
  //   cart.find().toArray((error, cartResults) => {

  //     let productIDs = []
  //     for (const item of cartResults) {
  //       productIDs.push(new mongo.ObjectID(item.product_id))
  //     }


  //     console.log(cartResults)
  //     products.find({ _id: { $in: productIDs } }).toArray((error, productResults) => {

  //     console.log(productResults)
  //       let results = []
  //       for (let i = 0; i < cartResults.length; i++) {
  //         results.push({ product: productResults[i], qty_cart: cartResults[i].qty_cart })
  //       }

  //       response.json(results)
  //     })
  //   })
  // })

  // app.post('/delete-one-in-cart', (request, response) => {
  //   cart.deleteOne({ "product._id": new mongo.ObjectID(request.body.id) })
  // })

  // app.get('/search-cart/:productID', (request, response) => {
  //   const id = new mongo.ObjectID(request.params.productID)
  //   cart.findOne({ "product._id": id }, (error, result) => {
  //     response.json(result)
  //   })
  // })

  // app.post('/empty-cart', (request, response) => {
  //   cart.deleteMany({})
  // })

  // app.post('/update-cart', (request, response) => {
  //   const id = new mongo.ObjectID(request.body.id)
  //   const qty = request.body.qty

  //   inventory.findOne({ product_id: id }, (error, result) => {
  //     if (result.qty_inventory < qty || qty === 0) {
  //       response.json({ success: false })
  //     }
  //     else {
  //       cart.update({ product_id: id }, { $set: { qty_cart: qty } })
  //       response.json({ success: true })
  //     }
  //   })
  // })

  // app.post('/save-order', (request, response) => {

  //   cart.find().toArray((error, cart) => {
  //     orders.insertOne({
  //       items: request.body.items,
  //       username: request.body.username,
  //       totalPrice: request.body.totalPrice
  //     })
  //   })
  // })

  // app.get('/orders', (request, response) => {

  //   orders.find().toArray((error, result) => {
  //     response.json(result)
  //   })
  // })

  // app.post('/signin', (request, response) => {
  //   const username = request.body.username
  //   const password = request.body.password
  //   console.log('p', password)
  //   console.log('username', username)
  //   users.findOne({ "username": username }, (error, user) => {

  //     if (user !== null && user.password === password) {
  //       jwt.sign({ user }, 'ijba0913', (error, token) => {
  //         response.json({ success: true, token: token })
  //       })
  //     }
  //     else {
  //       response.json({ success: false })
  //     }
  //   })
  // })

  // app.get('/user', (request, response) => {
  //   const authHeader = request.header('Authorization')

  //   jwt.verify(extractJWT(authHeader), 'ijba0913', (error, decoded) => {
  //     if (error) {
  //       response.json({ valid: false })
  //     }
  //     else {
  //       response.json({ currentUser: decoded.user, valid: true })
  //     }
  //   })

  // })

  // app.post('/signup', (request, response) => {

  //   users.findOne({ username: request.body.username }, (error, resultUser) => {
  //     if (resultUser === null) {
  //       users.insertOne({
  //         username: request.body.username,
  //         firstName: request.body.firstName,
  //         lastName: request.body.lastName,
  //         password: request.body.password,
  //         position: request.body.position
  //       }, (error, result) => {
  //         response.json({ valid: true })
  //       })
  //     }
  //     else {
  //       response.json({ valid: false })
  //     }
  //   })

  // })

  // app.get('/users', (request, response) => {
  //   users.find().toArray((error, result) => {
  //     response.json(result)
  //   })
  // })

  // app.get('/search-username/:username', (request, response) => {
  //   const username = request.params.username

  //   users.findOne({ username: username }, (error, result) => {
  //     response.json(result)
  //   })
  // })






