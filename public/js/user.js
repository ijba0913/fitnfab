function signInUser() {
  const input = {
    username: document.getElementById('signInUsername').value,
    password: document.getElementById('signInPassword').value
  }

  doAjax('POST', '/signIn', input, (result) => {
    console.log(result)
    if (result.success) {
      localStorage.setItem('theJwt', result.theToken)
      window.location.reload()
    }
    else {
      alert(result.message);
    }
  })
}

function signUpUser(user) {
  doAjax('POST', '/signUpUser', user, (result) => {
    console.log(user)
    console.log(result)
    window.alert(result.message)

    if (result.success) {
      window.location.reload()
    }
  })
}