function setAddToCartBtn(id, name) {
  doAjax('GET', '/getCartByProductId/' + id, {}, (cart) => {
    if (cart) {
      alert(`${name} is Already On the Cart`)
      document.getElementById('addToCartBtn').disabled = true
    }
    else {
      document.getElementById('addToCartBtn').disabled = false
    }
  })
}

function addToCart() {
  const id = document.getElementById('onViewProductId').value
  const name = document.getElementById('onViewProductName').value
  const qty_inventory = document.getElementById('onViewProductInvQty').value

  doAjax('POST', `/addToCart/` + id, { qty_inventory }, (result) => {
    if (result.success) {
      $('#editQuantityModal').modal('show')
      setEditCartModal(id, 1, name)
      displayCart()
    }
    else {
      alert(`${name} is ${result.message}`)
    }
  })
}

function displayCart() {
  doAjax('GET', '/getCart', {}, (cart) => {
    const container = document.getElementById('cartItemList')
    let string = ''

    for (const item of cart) {
      string += getCartProductHtml(item)
    }

    container.innerHTML = `<div class="list-group" id="cartTable">${string}</div>`
  })
}

function getCartProductHtml(item) {
  const id = item.product._id
  const name = item.product.name
  const price = item.product.price
  const qty = item.qty_cart

  return `<div class="row"> 
            <a href="#" class="list-group-item list-group-item-action col-9">
              <span class="row">
                <span class="col-10 text-right">${name}</span>
                <span class="col-2 text-right">${qty}</span>
              </span>
            </a>
            <button class="btn btn-sm btn-success col-1"  data-toggle="modal" data-target="#editQuantityModal"
              onclick="setEditCartModal('${id}', ${qty}, '${name}')">
              <img  class="glyph" src="glyph/svg/si-glyph-button-plus.svg"/>
            </button>
            <button class="btn btn-sm btn-danger col-1" onclick="deleteInCart('${id}')">
              <img  class="glyph" src="glyph/svg/si-glyph-trash.svg"/>
            </button>
          </div>`
}

function setEditCartModal(id, qty, name) {
  document.getElementById('editQuantityProductId').value = id
  document.getElementById('currentQuantity').value = qty
  document.getElementById('editQuantityValue').value = "1"
  document.getElementById('editQuantityModalTitle').innerHTML = name

  doAjax('GET', '/getInventoryByProductId/' + id, {}, (inventory) => {
    document.getElementById('inventoryInfo').innerHTML = `Only ${inventory.qty_inventory} Left`
    document.getElementById('editQuantityValue').max = `${inventory.qty_inventory}`
  })
}

function editProductCartQty() {
  const id = document.getElementById('editQuantityProductId').value
  const newQty = document.getElementById('editQuantityValue').value
  console.log(newQty, id)

  doAjax('POST', '/updateCartQty/' + id, { newQty }, (result) => {
    if (!result.success) {
      alert(result.message)
    }
  })
  displayCart()
}

function deleteInCart(id) {
  doAjax('POST', '/deleteProductInCart/' + id, {}, (result) => { })
  displayCart()
}

function emptyCart() {
  doAjax('POST', '/emptyCart', {}, (result) => { })
  displayCart()
}

function autoEmptyCart(user, showPage, command) {
  doAjax('GET', '/getCart', {}, (result) => {
    console.log(result.length)
    if (result.length > 0) {
      if (confirm("Are you sure to Exit? Cart is not empty. Cart will be cleared")) {
        emptyCart()
        showPage(user)
        command()
      }
      else {
        showBuyPage(user)
      }
    } else {
      showPage(user)
      command()
    }
  })
}