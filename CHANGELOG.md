<a name="1.0.0"></a>
# 1.0.0 (2017-11-04)


### Bug Fixes

* Change plus minus buttons into Modal ([aa1eee1](https://gitlab.com/ijba0913/fitnfab/commit/aa1eee1))
* Fix add to cart feature ([346bedc](https://gitlab.com/ijba0913/fitnfab/commit/346bedc))
* Fix bug to add product in cart ([c8aa88b](https://gitlab.com/ijba0913/fitnfab/commit/c8aa88b))
* Fix redirection of Post methods ([c5699ca](https://gitlab.com/ijba0913/fitnfab/commit/c5699ca))
* Fix the url in delete product feature ([b383442](https://gitlab.com/ijba0913/fitnfab/commit/b383442))
* Rearrange the commands ([007b930](https://gitlab.com/ijba0913/fitnfab/commit/007b930))


### Features

* **Add read items:** Items collection one to many read database ([f5a5c7b](https://gitlab.com/ijba0913/fitnfab/commit/f5a5c7b))
* Add items to cart ([4950a90](https://gitlab.com/ijba0913/fitnfab/commit/4950a90))
* **app.js:** Add read database feature ([23b00c2](https://gitlab.com/ijba0913/fitnfab/commit/23b00c2))
* Add a save order feature ([83764f3](https://gitlab.com/ijba0913/fitnfab/commit/83764f3))
* Add a validation for signup form ([f606ee3](https://gitlab.com/ijba0913/fitnfab/commit/f606ee3))
* Add ajax ([87069c6](https://gitlab.com/ijba0913/fitnfab/commit/87069c6))
* Add bootstrap, jQuery and popper ([4f17341](https://gitlab.com/ijba0913/fitnfab/commit/4f17341))
* Add button commands on click ([324e50b](https://gitlab.com/ijba0913/fitnfab/commit/324e50b))
* Add confirmation to delete a product ([59ba194](https://gitlab.com/ijba0913/fitnfab/commit/59ba194))
* Add delete button in inventory ([68854a2](https://gitlab.com/ijba0913/fitnfab/commit/68854a2))
* Add delete item in cart ([0bd3287](https://gitlab.com/ijba0913/fitnfab/commit/0bd3287))
* Add delete product feature ([b4ca888](https://gitlab.com/ijba0913/fitnfab/commit/b4ca888))
* Add feature to save user on sign up ([5ecdd3f](https://gitlab.com/ijba0913/fitnfab/commit/5ecdd3f))
* Add jwt feature ([acf1826](https://gitlab.com/ijba0913/fitnfab/commit/acf1826))
* Add modal view ([3c4ac5b](https://gitlab.com/ijba0913/fitnfab/commit/3c4ac5b))
* Add modals ([1bef476](https://gitlab.com/ijba0913/fitnfab/commit/1bef476))
* Add save order feature ([b135649](https://gitlab.com/ijba0913/fitnfab/commit/b135649))
* Add set container ([d32e262](https://gitlab.com/ijba0913/fitnfab/commit/d32e262))
* Add show orders fucntion ([cf578fe](https://gitlab.com/ijba0913/fitnfab/commit/cf578fe))
* Add sign page div ([437a92e](https://gitlab.com/ijba0913/fitnfab/commit/437a92e))
* Add signin ([5d5a1d3](https://gitlab.com/ijba0913/fitnfab/commit/5d5a1d3))
* Add Update quantity in inventory ([90e357f](https://gitlab.com/ijba0913/fitnfab/commit/90e357f))
* Add validation ([ec1475a](https://gitlab.com/ijba0913/fitnfab/commit/ec1475a))
* Add Validations to add a product in the cart ([036b45a](https://gitlab.com/ijba0913/fitnfab/commit/036b45a))
* **app.js:** Add save data feature ([5d8beae](https://gitlab.com/ijba0913/fitnfab/commit/5d8beae))
* **app.js:** Add search feature ([5c01215](https://gitlab.com/ijba0913/fitnfab/commit/5c01215))
* **app.js:** Add update product feature ([1124e59](https://gitlab.com/ijba0913/fitnfab/commit/1124e59))
* **html:** Add html and interface ([7423f57](https://gitlab.com/ijba0913/fitnfab/commit/7423f57))



<a name="1.0.0"></a>
# 1.0.0 (2017-11-02)


### Bug Fixes

* Change plus minus buttons into Modal ([aa1eee1](https://gitlab.com/ijba0913/fitnfab/commit/aa1eee1))
* Fix redirection of Post methods ([c5699ca](https://gitlab.com/ijba0913/fitnfab/commit/c5699ca))
* Fix the url in delete product feature ([b383442](https://gitlab.com/ijba0913/fitnfab/commit/b383442))
* Rearrange the commands ([007b930](https://gitlab.com/ijba0913/fitnfab/commit/007b930))


### Features

* **Add read items:** Items collection one to many read database ([f5a5c7b](https://gitlab.com/ijba0913/fitnfab/commit/f5a5c7b))
* Add modal view ([3c4ac5b](https://gitlab.com/ijba0913/fitnfab/commit/3c4ac5b))
* **app.js:** Add read database feature ([23b00c2](https://gitlab.com/ijba0913/fitnfab/commit/23b00c2))
* Add a save order feature ([83764f3](https://gitlab.com/ijba0913/fitnfab/commit/83764f3))
* Add a validation for signup form ([f606ee3](https://gitlab.com/ijba0913/fitnfab/commit/f606ee3))
* Add ajax ([87069c6](https://gitlab.com/ijba0913/fitnfab/commit/87069c6))
* Add bootstrap, jQuery and popper ([4f17341](https://gitlab.com/ijba0913/fitnfab/commit/4f17341))
* Add button commands on click ([324e50b](https://gitlab.com/ijba0913/fitnfab/commit/324e50b))
* Add delete button in inventory ([68854a2](https://gitlab.com/ijba0913/fitnfab/commit/68854a2))
* Add delete product feature ([b4ca888](https://gitlab.com/ijba0913/fitnfab/commit/b4ca888))
* Add feature to save user on sign up ([5ecdd3f](https://gitlab.com/ijba0913/fitnfab/commit/5ecdd3f))
* Add items to cart ([4950a90](https://gitlab.com/ijba0913/fitnfab/commit/4950a90))
* Add jwt feature ([acf1826](https://gitlab.com/ijba0913/fitnfab/commit/acf1826))
* **html:** Add html and interface ([7423f57](https://gitlab.com/ijba0913/fitnfab/commit/7423f57))
* Add modals ([1bef476](https://gitlab.com/ijba0913/fitnfab/commit/1bef476))
* **app.js:** Add save data feature ([5d8beae](https://gitlab.com/ijba0913/fitnfab/commit/5d8beae))
* **app.js:** Add search feature ([5c01215](https://gitlab.com/ijba0913/fitnfab/commit/5c01215))
* **app.js:** Add update product feature ([1124e59](https://gitlab.com/ijba0913/fitnfab/commit/1124e59))
* Add save order feature ([b135649](https://gitlab.com/ijba0913/fitnfab/commit/b135649))
* Add set container ([d32e262](https://gitlab.com/ijba0913/fitnfab/commit/d32e262))
* Add show orders fucntion ([cf578fe](https://gitlab.com/ijba0913/fitnfab/commit/cf578fe))
* Add sign page div ([437a92e](https://gitlab.com/ijba0913/fitnfab/commit/437a92e))
* Add signin ([5d5a1d3](https://gitlab.com/ijba0913/fitnfab/commit/5d5a1d3))
* Add Update quantity in inventory ([90e357f](https://gitlab.com/ijba0913/fitnfab/commit/90e357f))
* Add validation ([ec1475a](https://gitlab.com/ijba0913/fitnfab/commit/ec1475a))



<a name="1.0.0"></a>
# 1.0.0 (2017-11-02)


### Bug Fixes

* Change plus minus buttons into Modal ([aa1eee1](https://gitlab.com/ijba0913/fitnfab/commit/aa1eee1))
* Fix redirection of Post methods ([c5699ca](https://gitlab.com/ijba0913/fitnfab/commit/c5699ca))
* Rearrange the commands ([007b930](https://gitlab.com/ijba0913/fitnfab/commit/007b930))


### Features

* **Add read items:** Items collection one to many read database ([f5a5c7b](https://gitlab.com/ijba0913/fitnfab/commit/f5a5c7b))
* Add modal view ([3c4ac5b](https://gitlab.com/ijba0913/fitnfab/commit/3c4ac5b))
* **app.js:** Add read database feature ([23b00c2](https://gitlab.com/ijba0913/fitnfab/commit/23b00c2))
* Add a save order feature ([83764f3](https://gitlab.com/ijba0913/fitnfab/commit/83764f3))
* Add a validation for signup form ([f606ee3](https://gitlab.com/ijba0913/fitnfab/commit/f606ee3))
* Add ajax ([87069c6](https://gitlab.com/ijba0913/fitnfab/commit/87069c6))
* Add bootstrap, jQuery and popper ([4f17341](https://gitlab.com/ijba0913/fitnfab/commit/4f17341))
* Add button commands on click ([324e50b](https://gitlab.com/ijba0913/fitnfab/commit/324e50b))
* Add delete button in inventory ([68854a2](https://gitlab.com/ijba0913/fitnfab/commit/68854a2))
* Add delete product feature ([b4ca888](https://gitlab.com/ijba0913/fitnfab/commit/b4ca888))
* Add feature to save user on sign up ([5ecdd3f](https://gitlab.com/ijba0913/fitnfab/commit/5ecdd3f))
* Add items to cart ([4950a90](https://gitlab.com/ijba0913/fitnfab/commit/4950a90))
* Add jwt feature ([acf1826](https://gitlab.com/ijba0913/fitnfab/commit/acf1826))
* **html:** Add html and interface ([7423f57](https://gitlab.com/ijba0913/fitnfab/commit/7423f57))
* Add modals ([1bef476](https://gitlab.com/ijba0913/fitnfab/commit/1bef476))
* **app.js:** Add save data feature ([5d8beae](https://gitlab.com/ijba0913/fitnfab/commit/5d8beae))
* **app.js:** Add search feature ([5c01215](https://gitlab.com/ijba0913/fitnfab/commit/5c01215))
* **app.js:** Add update product feature ([1124e59](https://gitlab.com/ijba0913/fitnfab/commit/1124e59))
* Add save order feature ([b135649](https://gitlab.com/ijba0913/fitnfab/commit/b135649))
* Add set container ([d32e262](https://gitlab.com/ijba0913/fitnfab/commit/d32e262))
* Add show orders fucntion ([cf578fe](https://gitlab.com/ijba0913/fitnfab/commit/cf578fe))
* Add sign page div ([437a92e](https://gitlab.com/ijba0913/fitnfab/commit/437a92e))
* Add signin ([5d5a1d3](https://gitlab.com/ijba0913/fitnfab/commit/5d5a1d3))
* Add Update quantity in inventory ([90e357f](https://gitlab.com/ijba0913/fitnfab/commit/90e357f))
* Add validation ([ec1475a](https://gitlab.com/ijba0913/fitnfab/commit/ec1475a))



<a name="1.0.0"></a>
# 1.0.0 (2017-11-01)


### Bug Fixes

* Change plus minus buttons into Modal ([aa1eee1](https://gitlab.com/ijba0913/fitnfab/commit/aa1eee1))
* Fix redirection of Post methods ([c5699ca](https://gitlab.com/ijba0913/fitnfab/commit/c5699ca))


### Features

* **Add read items:** Items collection one to many read database ([f5a5c7b](https://gitlab.com/ijba0913/fitnfab/commit/f5a5c7b))
* Add delete button in inventory ([68854a2](https://gitlab.com/ijba0913/fitnfab/commit/68854a2))
* **app.js:** Add read database feature ([23b00c2](https://gitlab.com/ijba0913/fitnfab/commit/23b00c2))
* Add a save order feature ([83764f3](https://gitlab.com/ijba0913/fitnfab/commit/83764f3))
* Add ajax ([87069c6](https://gitlab.com/ijba0913/fitnfab/commit/87069c6))
* Add bootstrap, jQuery and popper ([4f17341](https://gitlab.com/ijba0913/fitnfab/commit/4f17341))
* Add button commands on click ([324e50b](https://gitlab.com/ijba0913/fitnfab/commit/324e50b))
* Add delete product feature ([b4ca888](https://gitlab.com/ijba0913/fitnfab/commit/b4ca888))
* Add feature to save user on sign up ([5ecdd3f](https://gitlab.com/ijba0913/fitnfab/commit/5ecdd3f))
* Add items to cart ([4950a90](https://gitlab.com/ijba0913/fitnfab/commit/4950a90))
* Add modal view ([3c4ac5b](https://gitlab.com/ijba0913/fitnfab/commit/3c4ac5b))
* Add modals ([1bef476](https://gitlab.com/ijba0913/fitnfab/commit/1bef476))
* Add save order feature ([b135649](https://gitlab.com/ijba0913/fitnfab/commit/b135649))
* Add set container ([d32e262](https://gitlab.com/ijba0913/fitnfab/commit/d32e262))
* Add show orders fucntion ([cf578fe](https://gitlab.com/ijba0913/fitnfab/commit/cf578fe))
* Add sign page div ([437a92e](https://gitlab.com/ijba0913/fitnfab/commit/437a92e))
* **app.js:** Add save data feature ([5d8beae](https://gitlab.com/ijba0913/fitnfab/commit/5d8beae))
* **app.js:** Add search feature ([5c01215](https://gitlab.com/ijba0913/fitnfab/commit/5c01215))
* **app.js:** Add update product feature ([1124e59](https://gitlab.com/ijba0913/fitnfab/commit/1124e59))
* Add Update quantity in inventory ([90e357f](https://gitlab.com/ijba0913/fitnfab/commit/90e357f))
* **html:** Add html and interface ([7423f57](https://gitlab.com/ijba0913/fitnfab/commit/7423f57))



<a name="1.0.0"></a>
# 1.0.0 (2017-10-28)


### Features

* Add ajax ([87069c6](https://gitlab.com/ijba0913/fitnfab/commit/87069c6))
* **app.js:** Add read database feature ([23b00c2](https://gitlab.com/ijba0913/fitnfab/commit/23b00c2))
* Add bootstrap, jQuery and popper ([4f17341](https://gitlab.com/ijba0913/fitnfab/commit/4f17341))
* Add button commands on click ([324e50b](https://gitlab.com/ijba0913/fitnfab/commit/324e50b))
* **html:** Add html and interface ([7423f57](https://gitlab.com/ijba0913/fitnfab/commit/7423f57))
* Add delete button in inventory ([68854a2](https://gitlab.com/ijba0913/fitnfab/commit/68854a2))
* **app.js:** Add save data feature ([5d8beae](https://gitlab.com/ijba0913/fitnfab/commit/5d8beae))
* **app.js:** Add search feature ([5c01215](https://gitlab.com/ijba0913/fitnfab/commit/5c01215))
* **app.js:** Add update product feature ([1124e59](https://gitlab.com/ijba0913/fitnfab/commit/1124e59))
* Add delete product feature ([b4ca888](https://gitlab.com/ijba0913/fitnfab/commit/b4ca888))
* Add modal view ([3c4ac5b](https://gitlab.com/ijba0913/fitnfab/commit/3c4ac5b))
* Add modals ([1bef476](https://gitlab.com/ijba0913/fitnfab/commit/1bef476))
* Add set container ([d32e262](https://gitlab.com/ijba0913/fitnfab/commit/d32e262))



<a name="1.0.0"></a>
# 1.0.0 (2017-10-28)


### Features

* Add ajax ([87069c6](https://gitlab.com/ijba0913/fitnfab/commit/87069c6))
* **app.js:** Add read database feature ([23b00c2](https://gitlab.com/ijba0913/fitnfab/commit/23b00c2))
* Add bootstrap, jQuery and popper ([4f17341](https://gitlab.com/ijba0913/fitnfab/commit/4f17341))
* Add button commands on click ([324e50b](https://gitlab.com/ijba0913/fitnfab/commit/324e50b))
* **html:** Add html and interface ([7423f57](https://gitlab.com/ijba0913/fitnfab/commit/7423f57))
* Add delete button in inventory ([68854a2](https://gitlab.com/ijba0913/fitnfab/commit/68854a2))
* **app.js:** Add save data feature ([5d8beae](https://gitlab.com/ijba0913/fitnfab/commit/5d8beae))
* **app.js:** Add search feature ([5c01215](https://gitlab.com/ijba0913/fitnfab/commit/5c01215))
* **app.js:** Add update product feature ([1124e59](https://gitlab.com/ijba0913/fitnfab/commit/1124e59))
* Add delete product feature ([b4ca888](https://gitlab.com/ijba0913/fitnfab/commit/b4ca888))
* Add modal view ([3c4ac5b](https://gitlab.com/ijba0913/fitnfab/commit/3c4ac5b))
* Add modals ([1bef476](https://gitlab.com/ijba0913/fitnfab/commit/1bef476))
* Add set container ([d32e262](https://gitlab.com/ijba0913/fitnfab/commit/d32e262))



